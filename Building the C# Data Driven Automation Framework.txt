********
*ReadMe*
********

@Author: lmangoua
Date: 21/08/2018

Task 2 of Absa Assessment
-------------------------
This is a C# based Data Driven Framework i built. 
I used Miscrosoft Visual Studio Enterprise 2017, Version 15.7.6 for this project.

We have 2 projects in that solution.

1- Project 1: AutomationFramework

 There i have:
 - PageObjects > AddUserPageObject.cs
   In that class i am storing the page objects of element related to that test case.

 - Utilities > SeleniumDriverUtility.cs
   In that class i setup the Driver, and also store my custom methods used for actions.

 - TestClasses > Reusable > NavigateToURL.cs
   In the Reusable folder i am storing Reusable classes, like the class to navigate to a particular URL.

 - TestClasses > AddUser_Absa.cs
   In that class i have the logic for the "Add User" test case.

2- Project 2: AutomationTest 

 Here is a Unit test project, which i use to execute test.

 I have:
 - Core > Baseclass.cs
 In that class i initialize the Driver and also close the browser after each test finish to execute.

 - Reports > ExtentReports.cs
 In that class i set up the code to generate extent reports.

 - TestReports
 That folder is set up to store extentReports.html files.
 To access them just right click the folder, then select "Open folder in File Explorer".

 - TestSuites > AddUser_AbsaTestSuite.cs
 In that class i execute the test suite for the "Add User" test case and also set up the parameters to use in the logic class.
 THIS IS WERE WE EXECUTE THE CODE.
***************************************************************************************
* TO RUN THE TEST CASE "ADD USER" WE EXECUTE THE TESTSUITE "AddUser_AbsaTestSuite.cs".*
***************************************************************************************

************************************************************************************************************************************
Building the C# Data Driven Automation Framework
------------------------------------------------

1. Create a new Project Selection
 a- New Project > Visual C# > Class Library(.Net framework) (e.g: AutomationFramework)
 b- Inside the same solution add a new project:
    Add > New Project > Visual C# > Test > Unit Test Project (e.g: AutomationTest)

2. In "AutomationTest" project
 a- Right click "Reference"
 b- Add Reference
 c- Select checkbox: Projects > Solution > AutomationFramework 
    (It is done to map that project to the current)
 d- Right click "Reference"
 e- Manage NuGet Packages
 f- Install packages:
    -Selenium.WebDriver v3.13.1
    -Selenium.WebDriver.ChromeDriver
    -MSTest.TestAdapter v1.2.1
    -MSTest.TestFramework
    -ExtentReports v2.40.1
    -ExcelDataReader v3.4.0

3. In "AutomationFramework" project
 a- Right click "Reference"
 b- Manage NuGet Packages
 c- Install packages:
    -Selenium.Support v3.13.1
    -Selenium.WebDriver v3.13.1
    -Selenium.WebDriver.ChromeDriver
    -DotNetSeleniumExtras.PageObjects
    -DotNetSeleniumExtras.PageObjects.Core
    -DotNetSeleniumExtras.WaitHelpers
    -MSTest.TestAdapter v1.2.1
    -MSTest.TestFramework

NB: 
--  On Mac the Unit test part of this project won't work. U gonna get warning message "this project is not supported by visual studio community 2017 for mac".


"I had this problem as well. The problem was the Unit Test project that I had in my solution. I moved to xUnit (add new project, move the source files, edit them to conform to xUnit, remove old project), and then everything was fine."


