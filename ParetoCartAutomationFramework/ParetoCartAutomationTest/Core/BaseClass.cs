﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParetoCartAutomationFramework.Entities;
using ParetoCartAutomationFramework;

/*
 * @author: lmangoua
 * Date: 04/07/2018
 * This Class is use for common methods over the project, to avoid repetition
 * Notes: Install "NUnit" package in the Reference
 * */

namespace ParetoCartAutomationTest.Core
{
    public class BaseClass
    {
        private Enums.BrowserType Chrome;
        private Enums.BrowserType IE;
        private Enums.BrowserType Firefox;
        private Enums.BrowserType Safari;

        //StartDriver
        [TestInitialize]
        public void Init()
        {
            //To initialize the driver before it runs
            SeleniumDriverUtility.StartDriver();
        }

        //Close browser
        [TestCleanup]
        public void CleanUp()
        {
            SeleniumDriverUtility.Close();
        }
    }
}
