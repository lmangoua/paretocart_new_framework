﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using ParetoCartAutomationFramework;
using ParetoCartAutomationTest.TestSuites.ChromeBrowser.Reusables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParetoCartAutomationTest.TestSuites.ChromeBrowser.Users
{
    [TestFixture]
    public class CreateContact
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        public ChromeDriver Driver { get; private set; }

        [SetUp]
        #region SetupTest
        public void SetupTest()
        {
            //driver = new ChromeDriver();
            var options = new ChromeOptions();
            options.AddArguments("disable-infobars"); //To remove message "chrome is being controlled by automated test software"
            options.AddArguments("--start-maximized"); //To maximize browser
            Driver = new ChromeDriver(@"D:\Softwares\Selenium Frameworks\Creating Automated Framework With Selenium\AutomationFreamework\packages", options);
            SeleniumDriverUtility.Wait(TimeSpan.FromSeconds(5));
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }
        #endregion

        [TearDown]
        #region TeardownTest
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            NUnit.Framework.Assert.AreEqual("", verificationErrors.ToString());
        }

        #endregion

        //[Test]
        //#region CreateContact
        ////NavigateToUrlAndLogin.Login();
        //#endregion
    }
}
