﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParetoCartAutomationTest.Core;
using ParetoCartAutomationFramework.TestClasses.Reusable;

/*
 * @author: lmangoua
 * Date: 09/07/2018
 * */

namespace ParetoCartAutomationTest
{
    [TestClass]
    public class NavigateToURLAndLoginTestSuite : BaseClass //To inherit from BaseClass (Same as doing "LoginTestSuite extends BaseClass {} in java")
    {
        //Loging to Page
        [TestMethod]
        public static void LoginToParetoCart()
        { 
            //Open browser and to to URL
            Login.GoToURL();

            ////Hover over the lock icon
            Login.NavigateToLoginPage();

            //Hover over the lock icon 
            Login.LoginPage();             
        }
    }
}
