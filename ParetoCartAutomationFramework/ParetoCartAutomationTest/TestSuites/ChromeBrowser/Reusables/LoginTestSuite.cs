﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParetoCartAutomationTest.Core;
using ParetoCartAutomationFramework.TestClasses.Reusable;
using ParetoCartAutomationFramework.Utilities;

/*
 * @author: lmangoua
 * Date: 09/07/2018
 * */

namespace ParetoCartAutomationTest
{
    [TestClass]
    public class LoginTestSuite : BaseClass //To inherit from BaseClass (Same as doing "LoginTestSuite extends BaseClass {} in java")
    {
        //Loging to Page
        [TestMethod]
        public void LoginToParetoCart()
        {
            ExcelLibrary.PopulateInCollection(@"D:\Documents\ParetoCart\ParetoCartAutomationFramework\TestPacks\TestData.xlsx");

            ////Open browser and to to URL
            //Login.GoToURL();

            //Open browser and to to URL
            Login.GoToURL();

            ////Hover over the lock icon
            Login.NavigateToLoginPage();

            //Hover over the lock icon 
            Login.LoginPage();

            //Validate i logged in successfully
            //Assert.IsTrue(DashboardPage.IsAt, "Failed to Login."); //It is failing
            
        }
    }
}
