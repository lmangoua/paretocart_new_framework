﻿using OpenQA.Selenium;
using ParetoCartAutomationFramework.PageObjects;
using ParetoCartAutomationFramework.PageObjects.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * @author: lmangoua
 * Date: 09/07/2018
 * */

namespace ParetoCartAutomationFramework.TestClasses.Users
{
    public class CreateContact
    {
        public static IWebDriver driver { get; private set; }
        public static string userFirstName = "John";
        public static string userLastName = "Doe";
        public static string userEmail = "johnDoe@paretocart.com";
        public static string userFullName = userFirstName + " " + userLastName;

        #region SelectUsers
        public static void SelectUsers()
        {
            //Click Users icon
            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.UsersIconXpath());

            SeleniumDriverUtility.ClickElementByXpath(ReusablePageObjects.UsersIconXpath());

            Console.WriteLine("Selected Users successfully");
        }
        #endregion

        #region SelectUsers_Contacts
        public static void SelectUsers_Contacts()
        {
            //Click Contacts span
            SeleniumDriverUtility.WaitForElementByXpath(CreateContactPageObjects.ContactsSpanXpath());

            SeleniumDriverUtility.ClickElementByXpath(CreateContactPageObjects.ContactsSpanXpath());

            Console.WriteLine("Clicked Contact successfully");
        }
        #endregion

        #region AddContact
        public static void AddContact()
        {  
            //Click Add Contact btn
            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.ContactsPageLabelXpath());

            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.AddContactButtonXpath());

            SeleniumDriverUtility.ClickElementByXpathUsingAction(ReusablePageObjects.AddContactButtonXpath());

            /*Add contact details*/
            //First Name
            SeleniumDriverUtility.WaitForElementByXpath(CreateContactPageObjects.FirstNameTextBoxXpath());

            SeleniumDriverUtility.EnterTextByXpath(CreateContactPageObjects.FirstNameTextBoxXpath(), userFirstName);

            //last Name
            SeleniumDriverUtility.WaitForElementByXpath(CreateContactPageObjects.LastNameTextBoxXpath());

            SeleniumDriverUtility.EnterTextByXpath(CreateContactPageObjects.LastNameTextBoxXpath(), userLastName);

            //Email
            SeleniumDriverUtility.WaitForElementByXpath(CreateContactPageObjects.EmailTextBoxXpath());

            SeleniumDriverUtility.EnterTextByXpath(CreateContactPageObjects.EmailTextBoxXpath(), userEmail);

            //Click Save btn
            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.SaveButtonXpath());

            SeleniumDriverUtility.ClickElementByXpath(ReusablePageObjects.SaveButtonXpath());

            Console.WriteLine("Added Contact successfully");
        }
        #endregion

        #region EnterDetail
        public static void EnterDetail()
        { 
            //Validate Detail page
            SeleniumDriverUtility.WaitForElementByXpath(CreateContactPageObjects.DetailPageLabelXpath());

            /*Fill in the Detail*/ 
            //First Name
            SeleniumDriverUtility.WaitForElementByXpath(CreateContactPageObjects.FirstNameTextBoxXpath());

            SeleniumDriverUtility.EnterTextByXpath(CreateContactPageObjects.FirstNameTextBoxXpath(), userFirstName);

            //last Name
            SeleniumDriverUtility.WaitForElementByXpath(CreateContactPageObjects.LastNameTextBoxXpath());

            SeleniumDriverUtility.EnterTextByXpath(CreateContactPageObjects.LastNameTextBoxXpath(), userLastName);

            //Email
            SeleniumDriverUtility.WaitForElementByXpath(CreateContactPageObjects.EmailDetailTextBoxXpath());

            SeleniumDriverUtility.EnterTextByXpath(CreateContactPageObjects.EmailDetailTextBoxXpath(), userEmail);

            //Click Save btn
            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.SaveButtonXpath());

            SeleniumDriverUtility.ClickElementByXpath(ReusablePageObjects.SaveButtonXpath());

            //Click Return btn
            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.ReturnButtonXpath());

            SeleniumDriverUtility.ClickElementByXpath(ReusablePageObjects.ReturnButtonXpath());

            Console.WriteLine("Entred Detail successfully");
        }
        #endregion

        #region ValidateContactCreated
        public static void ValidateContactCreated()
        {
            //Validate Name and Email
            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.NameLabelValidationXpath(userFullName));

            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.NameLabelValidationXpath(userEmail));

            Console.WriteLine("Validated Name: " + userFullName + " and Email: " + userEmail);
        }
        #endregion
    }
}
