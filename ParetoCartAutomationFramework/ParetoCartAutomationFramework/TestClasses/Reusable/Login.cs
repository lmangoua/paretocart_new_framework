﻿using System;
using ParetoCartAutomationFramework.PageObjects;
using OpenQA.Selenium;
using ParetoCartAutomationFramework.Utilities;

/*
 * @author: lmangoua
 * Date: 09/07/2018
 * */

namespace ParetoCartAutomationFramework.TestClasses.Reusable
{
    public class Login
    {
        public static IWebDriver driver { get; private set; }

        #region GoToURL
        public static void GoToURL()
        {
            try
            {
                SeleniumDriverUtility.Driver.Navigate().GoToUrl(SeleniumDriverUtility.BaseAddress);
            }
            catch (Exception e)
            {
                //Insert code for to display comment "("Failed to navigate to URL - " + e.getMessage())"
                Console.WriteLine("\nMessage ---\n{0}", e.Message);
            }

            //!Make sure to add the path to where you extracting the chromedriver.exe:            
            //var driver = new ChromeDriver(@"D:\Softwares\Selenium Frameworks\Creating Automated Framework With Selenium\AutomationFreamework\packages");  //("./AutomationFreamework/packages/Chrome")
            //var driver = new FirefoxDriver();
            //driver.Navigate().GoToUrl("http://10.11.1.56:82/");
        }
        #endregion

        #region Login  
        public static void LoginPage()
        {
            //Enter Email
            SeleniumDriverUtility.WaitForElementByXpath(LoginPageObjects.EmailTextboxXpath());

            //SeleniumDriverUtility.EnterTextByXpath(LoginPageObjects.EmailTextboxXpath(), "admin@changeme.com");
            SeleniumDriverUtility.EnterTextByXpath(LoginPageObjects.EmailTextboxXpath(), ExcelLibrary.ReadData(1, "Username"));

            //Enter Password
            SeleniumDriverUtility.WaitForElementByXpath(LoginPageObjects.PasswordTextboxXpath());

            //SeleniumDriverUtility.EnterTextByXpath(LoginPageObjects.PasswordTextboxXpath(), "pcadmin");
            SeleniumDriverUtility.EnterTextByXpath(LoginPageObjects.PasswordTextboxXpath(), ExcelLibrary.ReadData(1, "Password"));

            //Click login button
            SeleniumDriverUtility.WaitForElementByXpath(LoginPageObjects.LoginButtonXpath());

            SeleniumDriverUtility.ClickElementByXpath(LoginPageObjects.LoginButtonXpath());

            /*Validate We are logged in*/
            //Wait for Home icon
            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.HomeIconXpath());

            //Wait for Orders icon
            SeleniumDriverUtility.WaitForElementByXpath(ReusablePageObjects.OrdersIconXpath());
        }
        #endregion

        public static void NavigateToLoginPage()
        { 
            //Hover over the lock icon
            SeleniumDriverUtility.WaitForElementByXpath(LoginPageObjects.LockIconXpath());

            SeleniumDriverUtility.HoverOverElementAndClickSubElementXpath(LoginPageObjects.LockIconXpath(), LoginPageObjects.LoginOptionXpath()); 
        }

        public static LoginCommand LoginAs(string userName)
        {
            return new LoginCommand(userName);
        }

        public class LoginCommand
        {
            private readonly string userName;
            private string password;

            public LoginCommand(string userName)
            {
                this.userName = userName;
            }

            public LoginCommand WithPassword(string password)
            {
                this.password = password;
                return this;
            }

            //public void Login()
            //{
            //    var loginInput = SeleniumDriverUtility.Driver.FindElement(By.XPath("//label[text()='Email ']/../..//input"));
            //    var wait = new WebDriverWait(SeleniumDriverUtility.Driver, TimeSpan.FromSeconds(10));
            //    SeleniumDriverUtility.Wait(TimeSpan.FromSeconds(1));
            //    SeleniumDriverUtility.Pause(1000);
            //    var element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//label[text()='Email ']/../..//input")));
            //    loginInput.SendKeys(userName);

            //    var passwordInput = SeleniumDriverUtility.Driver.FindElement(By.XPath("//label[text()='Password ']/../..//input"));
            //    wait = new WebDriverWait(SeleniumDriverUtility.Driver, TimeSpan.FromSeconds(10));
            //    SeleniumDriverUtility.Pause(1000);
            //    element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//label[text()='Password ']/../..//input"))); 
            //    passwordInput.SendKeys(password);

            //    var loginButton = SeleniumDriverUtility.Driver.FindElement(By.XPath("//button[text()='Login']"));
            //    loginButton.Click();
            //}                       
        }
    }
}
